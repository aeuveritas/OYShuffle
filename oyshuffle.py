"""
MIT License

Copyright (c) 2017 Yeonghun Jeong

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import json
import os
import random
import time

class Menus:
    def __init__(self):
        self.menus = []

        self.menus.append({"name":"load", "msg":"Load participants"})
        self.menus.append({"name":"show", "msg":"Show participants"})
        self.menus.append({"name":"shuffle_ob", "msg":"Shuffle OB participants"})
        self.menus.append({"name":"shuffle_yb", "msg":"Shuffle YB participants"})
        self.menus.append({"name":"review", "msg":"Review result"})
        self.menus.append({"name":"clear", "msg": "Clear result"})

        self.menus.append({"name":"exit", "msg":"Exit"})

    def get(self):
        return self.menus

class View:
    def __init__(self, menus):
        self.menus = menus

    def show(self):
        print("Random Presentation Order - v 0.1")

        for idx, menu in enumerate(self.menus, start=1):
            print("{0}. {1}".format(idx, menu["msg"]))

    def interact(self):
        self.show()

        try:
            menu = int(input("menu: "))
        except ValueError:
            menu = 999

        if menu == len(self.menus):
            print("Bye~")

        return menu - 1

class Participants:
    def __init__(self):
        self.OB = None
        self.YB = None

        self.orderOB = []
        self.orderYB = []

    def load(self):
        print("Load participants")

        with open('oyb.json') as json_data:
            participants = json.load(json_data)

            self.OB = participants["OB"]
            self.YB = participants["YB"]

    def show(self):
        if not self.OB or not self.YB:
            self.load()

        self.printParticipants("OB")
        self.printParticipants("YB")

    def printParticipants(self, _type):
        print("{0}".format(_type))
        participants = None

        if _type == "OB":
            participants = self.OB
        elif _type == "YB":
            participants = self.YB

        for p in participants:
            print("  {0}: {1}".format(p["name"], p["topic"]))


    def select(self, participants, cnt):
        num = None
        candidate = None
        for i in range(len(participants)*10):
            num = random.randrange(0, len(participants))
            candidate = participants[num]
            print(
                "{0}- {1} : {2}".format(
                    cnt, candidate["name"], candidate["topic"]), end="\r")
            interval = (i / 100)
            time.sleep(interval)
            print("                                          ", end="\r")

        print(
            "{0}- {1} : {2}".format(
                cnt, candidate["name"], candidate["topic"]))

        return num

    def registerOrder(self, name, _type):
        participants = None
        order = None
        if _type is "OB":
            participants = self.OB
            order = self.orderOB
        elif _type is "YB":
            participants = self.YB
            order = self.orderYB

        for i, p in enumerate(participants):
            if p["name"] is name:
                order.append(i)
                break

    def shuffle(self, _type):
        if not self.OB or not self.YB:
            self.load()

        temp = None
        if _type == "OB":
            self.orderOB.clear()
            print("Shuffle OB")
            temp = self.OB.copy()
        elif _type == "YB":
            self.orderYB.clear()
            print("Shuffle YB")
            temp = self.YB.copy()

        for i in range(len(temp)):
            num = self.select(temp, i+1)
            name = temp[num]["name"]
            self.registerOrder(name, _type)
            del temp[num]

        print("Congratulations!")

    def review(self):
        if not self.orderOB:
            self.shuffle("OB")

        if not self.orderYB:
            self.shuffle("YB")

        self.printParticipantsByIdx("OB")
        self.printParticipantsByIdx("YB")

    def printParticipantsByIdx(self, _type):
        print("{0}".format(_type))
        participants = None
        order = None

        if _type == "OB":
            order = self.orderOB
            participants = self.OB
        elif _type == "YB":
            order = self.orderYB
            participants = self.YB

        for idx in order:
            p = participants[idx]
            print("  {0}: {1}".format(p["name"], p["topic"]))

    def clear(self):
        self.OB = None
        self.YB = None

        self.orderOB.clear()
        self.orderYB.clear()

        print("All data is cleared")

def main():
    menus = Menus()
    menuItems = menus.get()
    view = View(menuItems)
    participants = Participants()

    while True:
        os.system("clear")

        menuIdx = view.interact()

        try:
            menu = menuItems[menuIdx]
        except IndexError:
            menu = None

        if not menu:
            print("Please retry")
        elif menu["name"] == "exit":
            break
        elif menu["name"] == "load":
            participants.load()
        elif menu["name"] == "show":
            participants.show()
        elif menu["name"] == "shuffle_ob":
            participants.shuffle("OB")
        elif menu["name"] == "shuffle_yb":
            participants.shuffle("YB")
        elif menu["name"] == "review":
            participants.review()
        elif menu["name"] == "clear":
            participants.clear()

        _ = input()

if __name__ == "__main__":
    main()
